// gen-html.js
// Handles generation of HTML elements
//
//    lighttpd-deployable-site
//    Copyright (C) 2021  Grant Ward
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

const _ADD_TEXT_CLASS_RE = /gen-html-add-text-(?<property>\w+)/

function createElementWithText(tag, text) {
  //Build and return an HTML element with given text
  var element = document.createElement(tag);
  addTextToElement(element, text);
  return element;
}

function addTextToElement(element, text) {
  var text = document.createTextNode(text);
  element.appendChild(text);
}

function genElementFromTemplate(template, obj) {
  //Build and return an HTML element from the given template
  //using data from obj
  function populateElementText(element, obj) {
    //populate an element from an obj where requested
    var match = element.className.match(_ADD_TEXT_CLASS_RE);
    if (match) {
      //If element requests text property from obj, set it
      //TODO: behavior if text already present?
      var property = match['groups']['property'];
      addTextToElement(element, obj[property]);
    }
    for (var child=element.firstChild; child!==null; child=child.nextSibling) {
      //Iterate through child nodes to populate recrusively down
      populateElementText(child, obj);
    }
  }
  var element = template.cloneNode(true);
  populateElementText(element, obj);
  return element;
}
