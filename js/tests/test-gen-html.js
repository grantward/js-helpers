//node dependencies
var assert = require("assert").strict;

//local dependencies
var unittest = require("./unittest");

var TESTS = [
  test_createElementWithText,
];

function test_createElementWithText() {
  var element = createElementWithText("p", "Testing 1 2 3");
  assert.notEqual(element, null);
}

unittest.runTest(test_createElementWithText);
unittest.printResults();
