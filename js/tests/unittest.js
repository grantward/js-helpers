//Provides very simple unittesting framework for Javascript

var assert = require("assert").strict;


var RESULTS = {
  "success": 0,
  "fail": 0,
  "tests": [],
  "errors": {},
};

function runTest(func) {
  RESULTS.tests.push(String(func));
  try {
    func();
    RESULTS.success += 1;
  } catch (err) {
    RESULTS.fail += 1;
    RESULTS.errors[String(func)] = err;
  }
}

function printResults() {
  var total = RESULTS.success + RESULTS.fail;
  console.log("UNITTESTS RESULTS:");
  console.log("PASSED: [" + RESULTS.success + "/" + total + "]");
}

module.exports = {
  runTest,
  printResults,
};
